import java.util.Scanner;

public class LLFrequencyCounter{
    public static void main(String args[]){
	int minlen = Integer.parseInt(args[0]);
    
	LinkedListST<String, Integer> st =
	    new LinkedListST<String, Integer>();

	Scanner input = new Scanner(System.in);
    
	while(input.hasNext()){
	    String word = input.next();
      
	    if(word.length() < minlen) { continue; }
	    if(!st.contains(word)) { st.put(word, 1); }
	    else { st.put(word, st.get(word) + 1); }
	}
    
	String max = "";
	st.put(max, 0);
	for(String word : st.keys())
	    if(st.get(word) > st.get(max))
		max = word;
    
	System.out.println(max + " " + st.get(max));
    }
}
